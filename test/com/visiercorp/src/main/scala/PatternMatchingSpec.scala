package com.visiercorp.src.main.scala

import com.visiercorp.src.main.scala.PatternMatching._
import org.scalatest.{FunSpec, Matchers}

class PatternMatchingSpec extends FunSpec with Matchers {
  describe("match a player instance") {
    it("match cole 3") {
      PatternMatching.matchAPlayer(Player("Cole", 3)) shouldEqual "Cole 3"
    }

    describe("pattern match a player with jersey greater than 3") {
      it("does not match cole 3") {
        PatternMatching.matchAPlayerWithJerseyNumberGreaterThan3(Player("Cole", 3)) shouldEqual ""
      }
      it("match pogba 6") {
        PatternMatching.matchAPlayerWithJerseyNumberGreaterThan3(Player("Pogba", 6)) shouldEqual "Pogba 6"
      }
    }

    describe("pattern match a player with jersey equals to 3") {
      it("match cole 3") {
        PatternMatching.matchAPlayerWithJerseyNumberEquals3(Player("Cole", 3)) shouldEqual 3
      }
      it("does not match pogba 6") {
        PatternMatching.matchAPlayerWithJerseyNumberEquals3(Player("Pogba", 6)) shouldEqual -1
      }
    }
  }

  describe("naive leaf sum") {
    it("calculate based on different List values") {
      PatternMatching.leafSum(List(List(3, 8), 2, List(5))) shouldEqual 18
      PatternMatching.leafSum(List()) shouldEqual 0
      PatternMatching.leafSum(Nil) shouldEqual 0
    }

    it("nested Lists") {
      PatternMatching.leafSum(List(List(3, List(5, 8)), 2, List(5))) shouldEqual 23
    }
  }

  describe("leaf sum with case classes") {
    it("calculate based on different node patterns") {
      PatternMatching.leafSum(Node(Node(Leaf(3), Node(Leaf(8), Leaf(3))), Node(Leaf(2), Leaf(5)))) shouldEqual 21
      PatternMatching.leafSum(Node(Leaf(3), Leaf(4))) shouldEqual 7
      PatternMatching.leafSum(Leaf(1)) shouldEqual 1
      PatternMatching.leafSum(Node(Node(Node(Leaf(3), Leaf(9)), Node(Leaf(8), Leaf(3))), Node(Leaf(2), Leaf(5)))) shouldEqual 30
    }
  }

  describe("leaf sum with case classes with multiple nodes") {
    it("calculate based on different node patterns") {
      PatternMatching.leafSumForNodeWithMultipleChildren(
        NodeWithMultipleChildren(
          NodeWithMultipleChildren(
            Leaf(3),
            NodeWithMultipleChildren(Leaf(8), Leaf(3)),
            NodeWithMultipleChildren(
              Leaf(2), Leaf(5),
              NodeWithMultipleChildren(Leaf(7))
            )
          )
        )
      ) shouldEqual 28

      PatternMatching.leafSumForNodeWithMultipleChildren(NodeWithMultipleChildren()) shouldEqual 0

      PatternMatching.leafSumForNodeWithMultipleChildren(NodeWithMultipleChildren(Leaf(1), Leaf(3), Leaf(5))) shouldEqual 9
    }
  }

  describe("math operator tree") {
    it("calculate based on different node patterns") {
      PatternMatching.eval(
        OperatorNode(
          Plus,
          OperatorNode(Divide),
          OperatorNode(Multiply, ValueLeaf(3), ValueLeaf(8)),
          OperatorNode(Minus, ValueLeaf(5), ValueLeaf(3)),
          ValueLeaf(2),
          OperatorNode(Minus, ValueLeaf(5))
        )
      ) shouldEqual 23

      PatternMatching.eval(
        OperatorNode(Divide, ValueLeaf(99), ValueLeaf(3), ValueLeaf(11))
      ) shouldEqual 3
    }

    it("calculate trees with multiple levels") {
      PatternMatching.eval(
        OperatorNode(
          Plus,
          OperatorNode(Multiply, ValueLeaf(3), ValueLeaf(8)), //24
          OperatorNode(Minus, OperatorNode(Multiply, OperatorNode(Plus, ValueLeaf(2), ValueLeaf(3)), ValueLeaf(7)), ValueLeaf(5), ValueLeaf(3)), //27
          ValueLeaf(2), //2
          OperatorNode(Minus, ValueLeaf(5)) //-5
        )
      ) shouldEqual 48
    }

    it("no children underneath an operator should regturn 0") {
      PatternMatching.eval(
        OperatorNode(
          Plus
        )
      ) shouldEqual 0
    }

    it("one child under minus node should return negate the value") {
      PatternMatching.eval(
        OperatorNode(
          Minus, ValueLeaf(-22)
        )
      ) shouldEqual 22
    }

    it("multiple children under minus node should work properly") {
      PatternMatching.eval(
        OperatorNode(
          Minus, ValueLeaf(-22),
          ValueLeaf(5), ValueLeaf(-7)
        )
      ) shouldEqual -20
    }
  }
}
