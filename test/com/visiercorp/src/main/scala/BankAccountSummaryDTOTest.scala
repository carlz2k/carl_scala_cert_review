package com.visiercorp.src.main.scala

import com.visiercorp.src.main.scala.domain.models.BankAccount
import com.visiercorp.src.main.scala.transfers.BankAccountSummaryDTO
import org.scalatest.{FunSpec, Matchers}

class BankAccountSummaryDTOTest extends FunSpec with Matchers {
  it("Can be created from a specified name and balance.") {
    val bankAccountSummaryDTO = BankAccountSummaryDTO("name", 2.0)
    bankAccountSummaryDTO.name shouldEqual "name"
    bankAccountSummaryDTO.balance shouldEqual 2.0
  }

  describe("Created from a specified BankAccount") {

    it("Has name that is combined from the first and last name.") {
      val bankAccount = BankAccount("first", "last", 2.0)
      val bankAccountSummaryDTO =
        BankAccountSummaryDTO.fromBankAccount(bankAccount)
      bankAccountSummaryDTO.name shouldEqual bankAccount.firstName + " " + bankAccount.lastName
    }

    it("Has balance that is rounded up to 2 decimal places.") {

      val bankAccount = BankAccount("first", "last", 2.228)
      val bankAccountSummaryDTO =
        BankAccountSummaryDTO.fromBankAccount(bankAccount)
      bankAccountSummaryDTO.balance shouldEqual 2.23
    }

    it("Has balance that is rounded down to 2 decimal places.") {
      val bankAccount = BankAccount("first", "last", 2.223)
      val bankAccountSummaryDTO =
        BankAccountSummaryDTO.fromBankAccount(bankAccount)
      bankAccountSummaryDTO.balance shouldEqual 2.22
    }
  }

  it("Can be converted to JSON") {
    val bankAccountSummaryDTO = BankAccountSummaryDTO("name", 2.0)
    bankAccountSummaryDTO.toJSON().toString() shouldEqual "{\"name\":\"name\",\"balance\":2}"
  }

  it("Can be converted to JSON even with null name") {
    val bankAccountSummaryDTO = BankAccountSummaryDTO(null, 2.0)
    bankAccountSummaryDTO.toJSON().toString() shouldEqual "{\"name\":null,\"balance\":2}"
  }
}
