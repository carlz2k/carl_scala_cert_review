package com.visiercorp.src.main.scala

import org.scalatest.{Matchers, FunSpec}

class CollectionsSpec extends FunSpec with Matchers {
  describe("sort a vector") {
    it("empty vector") {
      CollectionsExec.sort(Vector()) shouldEqual Vector()
    }

    it("non empty vector") {
      CollectionsExec.sort(Vector(3.4f, 5.5f, 9.2f, 1.7f, 3.3f, 7.2f, 25.8f, 8f, 10.5f, 4.1f, 10.5f, 3.6f)) shouldEqual
          Vector(1.7f, 3.3f, 3.4f, 3.6f, 4.1f, 5.5f, 7.2f, 8.0f, 9.2f, 10.5f, 10.5f, 25.8f)
    }
  }

  describe("get first n value") {

    it("empty list") {
      CollectionsExec.getFirstNValues(List(), 3) shouldEqual List()
    }

    it("non empty list with n < list length") {
      CollectionsExec.getFirstNValues(List(1, 2), 3) shouldEqual List(1, 2)
    }

    it("non empty list with n = list length") {
      CollectionsExec.getFirstNValues(List(1, 2, 3), 3) shouldEqual List(1, 2, 3)
    }

    it("non empty list with n > list length") {
      CollectionsExec.getFirstNValues(List(1, 2, 3, 4), 3) shouldEqual List(1, 2, 3)
    }
  }

  describe("get from a map") {
    it("key exists") {
      CollectionsExec.get(Map("key" -> 5, "key2" -> 6), "key2") shouldEqual 6
    }

    it("key does not exists") {
      CollectionsExec.get(Map("key" -> 5, "key3" -> 6), "key2") shouldEqual 0
    }
  }

  describe("find odd numbers in a list") {
    it("empty list") {
      CollectionsExec.findOddsOnly(List()) shouldEqual List()
    }

    it("non empty list") {
      CollectionsExec.findOddsOnly(List(2, 5, 7, 4)) shouldEqual List(5, 7)
    }

    it("non empty list with only evens") {
      CollectionsExec.findOddsOnly(List(2, 4)) shouldEqual List()
    }
  }

  describe("merge two lists") {
    it("normal test") {
      CollectionsExec.merge(List(3, 5, 7), List(7, 5, 3)) shouldEqual List(3, 5, 7, 7, 5, 3)
    }
  }

  describe("find all values with keys") {
    it("normal test") {
      CollectionsExec.transform(Array("Tom", "Fred", "Harry"), Map("Tom" -> 3, "Dick" -> 4, "Harry" -> 5)) shouldEqual Array(3, 5)
    }

    it("test with keys that do not exist") {
      CollectionsExec.transform(Array("Tom", "Fred", "Harry"), Map("Tom1" -> 3, "Dick1" -> 4, "Harry1" -> 5)) shouldEqual Array()
    }
  }

  describe("mkstring with reduceleft") {
    it("empty string list") {
      CollectionsExec.mkString(Array()) shouldEqual ""
    }

    it("non empty list") {
      CollectionsExec.mkString(Array("1", "2", "3")) shouldEqual "123"
    }
  }

  describe("exercize 10") {
    it("normal test") {
      val n = 100000
      CollectionsExec.getLetterFrequencyMap(List.fill(n)("e").reduceLeft(_ + _)) shouldEqual Map('e' -> n)
    }
  }

}