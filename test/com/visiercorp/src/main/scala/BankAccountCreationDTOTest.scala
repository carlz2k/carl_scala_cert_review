package com.visiercorp.src.main.scala

import com.visiercorp.src.main.scala.domain.models.BankAccount
import com.visiercorp.src.main.scala.transfers.{BankAccountCreationDTO, BankAccountSummaryDTO}
import org.scalatest.{FunSpec, Matchers}
import play.api.libs.json.{Json, Writes}


class BankAccountCreationDTOTest extends FunSpec with Matchers {

  private val jsonWriter = Json.writes[BankAccountCreationDTO]

  describe("A BankAccountCreationDTO") {
    it("Can be created from valid JSON") {
      val bankAccountCreationDTOOriginal = BankAccountCreationDTO("first", "last", 2.5)
      BankAccountCreationDTO.fromJSON(
        jsonWriter.writes(bankAccountCreationDTOOriginal)
      ) shouldEqual bankAccountCreationDTOOriginal

    }

    it("Cannot be created from invalid JSON") {
      val badJsonValue = "{\"firstname\":\"first\",\"lastName\":\"last\",\"balance\":2.5}"
      val caught = intercept[Exception] {
        BankAccountCreationDTO.fromJSON(Json.parse(badJsonValue))
      }
      caught.getMessage shouldEqual "unable to convert to BankAccountCreationDTO"
    }

    it("Can be used to create a BankAccount") {
      val bankAccountCreationDTOOriginal = BankAccountCreationDTO("first", "last", 2.5)
      bankAccountCreationDTOOriginal.toBankAccount shouldEqual BankAccount("first", "last", 2.5)
    }
  }
}
