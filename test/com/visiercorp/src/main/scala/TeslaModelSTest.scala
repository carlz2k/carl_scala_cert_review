package com.visiercorp.src.main.scala

import com.visiercorp.src.main.scala.domain.models.{DoubleMotor, LargeBattery, TeslaModelS}
import org.scalatest.{FunSpec, Matchers}

class TeslaModelSTest extends FunSpec with Matchers {

  describe("A Tesla Model S") {
    val modelS: TeslaModelS = new TeslaModelS

    it("has a Model S name") {
      assert(modelS.name === "Tesla Model S")
    }

    it("has a medium battery capacity") {
      assert(modelS.chargeCapacity === 10)
    }

    it("has a single motor") {
      modelS.numberOfMotors shouldEqual 1
    }

    it("Can be upgraded with a large battery") {
      val upgradedTelsa = new TeslaModelS with LargeBattery
      upgradedTelsa.chargeCapacity shouldEqual 15
      upgradedTelsa.numberOfMotors shouldEqual 1
    }

    it("Can be upgraded with a large battery and a dual motor") {
      val upgradedTelsa = new TeslaModelS with LargeBattery with DoubleMotor
      upgradedTelsa.chargeCapacity shouldEqual 15
      upgradedTelsa.numberOfMotors shouldEqual 2
    }
  }
}