package com.visiercorp.src.main.scala

import org.scalatest.{Matchers, FunSpec}

class IterativeToFunctionalSpec extends FunSpec with Matchers {

  describe("reverse") {
    it("empty string") {
      IterativeToFunctional.reverse("") shouldEqual ""
    }

    it("non empty string") {
      IterativeToFunctional.reverse("abcd") shouldEqual "dcba"
    }

  }

  describe("foo2") {
    it("empty string") {
      IterativeToFunctional.foo2("") shouldEqual false
    }

    it("false case") {
      IterativeToFunctional.foo2("akkb") shouldEqual false
    }

    it("true case") {
      IterativeToFunctional.foo2("akkaa") shouldEqual true
    }
  }

  describe("foo3") {
    def foo3(strings: Array[String]) = {
      val newString = new Array[String](strings.length * 2)
      var i = 0
      while ( {i < strings.length}) {
        newString(2 * i) = strings(i)
        newString(2 * i + 1) = strings(i)

        {i += 1; i - 1}
      }
      newString
    }

    it("empty array") {
      IterativeToFunctional.foo3(Array()) shouldEqual foo3(Array())
    }
    it("normal test") {
      val array = (1 to 50).map(_.toString).toArray
      IterativeToFunctional.foo3(array) shouldEqual foo3(array)
    }
  }

  describe("foo4") {
    def foo4(names: Seq[String], numbers: Seq[Int]): Seq[(String, Int)] = {
      // names and numbers are known to have the same length
      import scala.collection.mutable.ListBuffer
      val list = new ListBuffer[(String, Int)]
      for (i <- 0 until names.length) {
        list += ((names(i), numbers(i)))
      }
      list
    }

    it("empty array") {
      IterativeToFunctional.foo4(List(), List()) shouldEqual foo4(List(), List())
    }
    it("normal test") {
      val numberList = 1 to 50
      val stringList = (51 to 100).map(_.toString)
      IterativeToFunctional.foo4(stringList, numberList) shouldEqual foo4(stringList, numberList)
    }

  }
}
