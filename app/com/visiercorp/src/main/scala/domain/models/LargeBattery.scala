package com.visiercorp.src.main.scala.domain.models

trait LargeBattery extends Chargeable {
  this: Car =>
  override val chargeCapacity: Double = 15
}
