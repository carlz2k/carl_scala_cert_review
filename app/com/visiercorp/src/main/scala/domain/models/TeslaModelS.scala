package com.visiercorp.src.main.scala.domain.models

class TeslaModelS extends Car with MediumBattery with SingleMotor {
  override val name: String = "Tesla Model S"
}
