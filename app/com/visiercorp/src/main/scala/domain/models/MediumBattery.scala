package com.visiercorp.src.main.scala.domain.models

trait MediumBattery extends Chargeable {
  self: Car =>

  override val chargeCapacity: Double = 10
}
