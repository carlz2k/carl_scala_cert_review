package com.visiercorp.src.main.scala

object OptionsExec {
  //Q9
  def sumNonNones(list: List[Option[Int]]): Int = {
    //TODO you can use flatten to get rid of Nones, and sum instead of foldLeft
    list.filter(_.isDefined).foldLeft(0)(_ + _.get)
  }


  //Q10a
  def compose(f: Double => Option[Double], g: Double => Option[Double]): Double => Option[Double] = {
    x: Double =>
      g(x) match {
        case Some(gx) => f(gx)
        case None => None
      }

  }

  //Q10b
  def composeWithFlatMap(f: Double => Option[Double], g: Double => Option[Double]): Double => Option[Double] = {
        //https://danielwestheide.com/blog/2012/12/19/the-neophytes-guide-to-scala-part-5-the-option-type.html
        //explains how flatmap on options works
    x: Double => g(x).flatMap(f)
  }
}
