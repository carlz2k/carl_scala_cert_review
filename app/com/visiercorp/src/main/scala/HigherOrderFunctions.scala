package com.visiercorp.src.main.scala

object HigherOrderFunctions {
  //Q5
  def largest(fun: Int => Int, inputs: Seq[Int]): Int = {
    //TODO Seq has a method max, which can be used in place of folding
    inputs.foldLeft(Int.MinValue)(
      (max, value) => {
        math.max(fun(value), max)
      }
    )
  }

  //Q6

  def largestAt(fun: Int => Int, inputs: Seq[Int]): Int = {
    inputs.foldLeft((Int.MinValue, -1))(
      (max, value) => {
        val temp = fun(value)
        if (temp >= max._1) {
          (temp, value)
        } else {
          max
        }
      }
    )._2
  }

  //Q7

  def adjustToPair(fun: (Int, Int) => Int): ((Int, Int)) => Int = {
    //TODO an anonymous function like this should have its parameter types declared (eg (x: (Int, Int)) => ...
    x => {
      //TODO for a simple function like this, we can omit the curly braces and put the body on the same line
      fun(x._1, x._2)
    }
  }
}
