package com.visiercorp.src.main.scala

object JavaInterop {
  //Q7
  //instread playing with generics, assume it is an array of integers
  //TODO you can stick with a generic array, you'll need to declare a type variable like def distinct[T]
  def distinct(array: Array[Int]): Array[Int] = {
    array match {
        //TODO Null match not needed
      case null => Array()
      case _ => array.distinct
    }
  }

  val AmericanTimeZonePrefix = "America/"

  //Q9
  def getAmericanTimezones: Array[String] = {
    java.util.TimeZone.getAvailableIDs.filter(
      //TODO This filter will give a false positive if the text America/ appears anywhere else in the string.
      // You may want to consider startsWith, and avoiding the toLowerCase calls
      _.toLowerCase().contains(AmericanTimeZonePrefix.toLowerCase())
    ).map(
      //TODO This will remove all instances of America/, instead of just the prefix
      _.replaceAll(AmericanTimeZonePrefix, "")
    ).sortWith(_ < _)
    //TODO You can use sorted here, so you don't need to pass _ < _
  }
}