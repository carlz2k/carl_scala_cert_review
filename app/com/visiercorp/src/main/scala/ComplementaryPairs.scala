package com.visiercorp.src.main.scala

import scala.collection.mutable

object ComplementaryPairs {
  def solution(K: Int, A: Vector[Int]): Int = {

    val map: mutable.HashMap[Int, Int] = new mutable.HashMap()
    //use mutable to meet O(n) space requirement
    //TODO Shouldn't need any mutable collections or vars for this question - have you encountered the groupBy method?

    A.foreach(
      x => {
        map.put(x, map.getOrElse(x, 0) + 1)
      }
    )

    A.foldLeft(0)(
      (acc, x) => {
        acc + map.getOrElse(K - x, 0)
      }
    )
  }
}

