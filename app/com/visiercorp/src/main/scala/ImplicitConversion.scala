package com.visiercorp.src.main.scala

//Q
class Complex(val real: Int, val imaginary: Int) {

}

object ImplicitConversion {
  implicit def convert(value: Int): Complex = new Complex(value, 0)

  // TODO This method should probably be in the Complex class
  def add(complex1: Complex, complex2: Complex): Complex = {
    new Complex(complex1.real + complex2.real, complex1.imaginary + complex2.imaginary)
  }

  def multiply(x: Int)(implicit by: Int): Int = x * by
}