package com.visiercorp.src.main.scala

object CollectionsExec {
  //Q0a
  def sort(vector: Vector[Float]): Vector[Float] = {
    //do I need to write out the sorting function on my own or use the out of box "sort with" or "sorted" function?
    //TODO You don't even need the _ < _ if you use sorted instead of sortWith (if the out of the box works, we prefer to use it)
    vector.sortWith(_ < _)
  }

  //Q0b
  //TODO this question is looking to get the first 3 values, instead of the first n values
  def getFirstNValues(list: List[Int], n: Int): List[Int] = {
    list.take(n)
  }

  //Q0c
  def get(map: Map[String, Int], key: String): Int = {
    map.getOrElse(key, 0)
  }

  //Q0d
  def findOddsOnly(list: List[Int]): List[Int] = {
    list.filter(_ % 2 != 0)
  }

  //Q0e
  def merge(list: List[Int], list2: List[Int]): List[Int] = {
    list ++ list2
  }

  //Q0f
  def append(list: List[Int], x: Int): List[Int] = {
    list :+ x //or use ListBuffer for better performance
  }

  //Q4
  def transform(keySet: Array[String], map: Map[String, Int]): Array[Int] = {
    keySet.flatMap(map.get)
  }

  //Q5
  def mkString(strings: Array[String]): String = {
    strings match {
      case Array() => ""
      case _ => strings.reduceLeft(_ + _) //or use fold left to avoid empty check
    }
  }

  //Q9
  //SynchronizedMap makes sure two threads cannot update the same key/value pair at the same time,
  //but it still cannot prevent the race condition in this problem because ideally we want the second read and write happens after the first read and write,
  //but int the original solution the first and second thread still can read the same value and update with the same value simultaneously
  //ConcurrentMap is more efficient than SynchronizedMap but it still won't resolve the race condition
  //we will have to manually synchronize the read and write block

  //Q10
  def getLetterFrequencyMap(str: String): Map[Char, Int] = {
    str.par.aggregate(Map[Char, Int]())(
      (m, c) => m + (c -> (m.getOrElse(c, 0) + 1)),
      //TODO when you use the curly braces, then we put the anonymous function body on a new line
      (m1, m2) => m1 ++ m2.map { case (k, v) => k -> (v + m1.getOrElse(k, 0)) }
    )
  }
}