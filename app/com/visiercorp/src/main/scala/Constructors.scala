package com.visiercorp.src.main.scala

//Q8a

class Car {
  // TODO You can make these fields part of the primary constructor
  private var manufacturer: String = ""
  private var name: String = ""
  private var modelYear: Int = -1
  var licensePlate: String = ""
  // None of these fields needs to be a var

  //primary constructor that can be called by other constructors with hidden default values
  def this(manufacturer: String, name: String, modelYear: Int, licensePlate: String) {
    this()
    this.manufacturer = manufacturer
    this.name = name
    this.modelYear = modelYear
    this.licensePlate = licensePlate
  }

  def this(manufacturer: String, name: String) {
    this(manufacturer, name, -1, "")
  }


  def this(manufacturer: String, name: String, modelYear: Int) {
    this(manufacturer, name, modelYear, "")
  }


  def this(manufacturer: String, name: String, licensePlate: String) {
    this(manufacturer, name, -1, licensePlate)
  }

  // If you can make the fields public vals, you won't need these getters
  def getManufacturer: String = {
    manufacturer
  }

  def getName: String = {
    name
  }

  def getModelYear: Int = {
    modelYear
  }
}

//Q8b

class CarAlt(
    val manufacturer: String,
    val name: String,
    val modelYear: Int = -1,
    var licensePlate: String = "") {
  // TODO You don't need to define this extra constructor, you can pass the parameters with their names
  def this(manufacturer: String, name: String, licensePlate: String) {
    this(manufacturer, name, -1, licensePlate)
  }
}

//Q10
//more lines of code compared to the original one
//TODO you can add parameters to the default constructor here
class Employee {
  // TODO these can be assigned from parameters to the default constructor
  private var name: String = "John Q. Public"
  var salary = 0.0

  // TODO Shouldn't need this alternative constructor
  def this(n: String, s: Double) {
    this()
    this.name = n
    this.salary = s
  }

  // TODO Shouldn't need this getter
  def getName: String = name
}
