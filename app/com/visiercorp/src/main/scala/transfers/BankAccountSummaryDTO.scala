package com.visiercorp.src.main.scala.transfers

import java.math.MathContext

import com.visiercorp.src.main.scala.domain.models.BankAccount
import play.api.libs.json._

/**
  * The DTO used to summarize a bank account.
  *
  * @param name    The full name of the bank account owner (combined first and last name).
  * @param balance The bank account balance (rounded to 2 decimal places).
  */
case class BankAccountSummaryDTO(name: String, balance: Double) {
  //TODO this can be stored in the BankAccountSummaryDTO object, since we don't need one of them per class instance.
  private lazy val jsonWriter: Writes[BankAccountSummaryDTO] = Json.writes[BankAccountSummaryDTO]

  /**
    * Returns the JSON presentation of the object.
    *
    * @return The JsValue.
    */
  def toJSON(): JsValue = jsonWriter.writes(this)
}

object BankAccountSummaryDTO {

  //private lazy val jsonWriter: Writes[BankAccountSummaryDTO] = Json.writes[BankAccountSummaryDTO]

  /**
    * Creates the corresponding BankAccountSummaryDTO from a specified BankAccount.  The balance is converted into a
    * Double with a precision of two decimal places.
    *
    * @param bankAccount The BankAccount to create the BankAccountSummaryDTO from.
    */
  def fromBankAccount(bankAccount: BankAccount): BankAccountSummaryDTO =
    BankAccountSummaryDTO(
      //TODO quasiquotes are a nice way to handle string combinations: s"myValue was equal to: ${myValue}"
      bankAccount.firstName + " " + bankAccount.lastName,
      bankAccount.balance.setScale(2, BigDecimal.RoundingMode.HALF_EVEN).toDouble
    )
}
