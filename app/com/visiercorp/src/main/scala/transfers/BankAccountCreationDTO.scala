package com.visiercorp.src.main.scala.transfers

import com.visiercorp.src.main.scala.domain.models.BankAccount

/**
  * The DTO used for bank account creation.
  *
  * @param firstName The bank account owner's first name.
  * @param lastName  The bank account owner's last name.
  * @param balance   The initial bank account balance.
  */
case class BankAccountCreationDTO(firstName: String, lastName: String, balance: Double) {

  /**
    * Returns the server-side domain representation of the BankAccountCreationDTO.
    *
    * @return BankAccount.
    */
  def toBankAccount(): BankAccount = BankAccount(firstName, lastName, balance)
}

object BankAccountCreationDTO {

  import play.api.libs.json._

  private lazy val jsonReader: Reads[BankAccountCreationDTO] = Json.reads[BankAccountCreationDTO]

  /**
    * Converts specified JSON into BankAccountCreationDTO.  Throws exception if JSON cannot be converted.
    *
    * @param json The JsValue to convert.
    * @return BankAccountCreationDTO.
    */
  def fromJSON(json: JsValue): BankAccountCreationDTO = {
    val result = jsonReader.reads(json)
    //TODO A nice alternative to if-checking for Option, JsResult, or other types where you can get a result or a
    // failure is .getOrElse(<whatever your backup behaviour is>).
    //thanks! learned some new syntax
    result.getOrElse(throw new Exception("unable to convert to BankAccountCreationDTO"))
  }

}
