package com.visiercorp.src.main.scala

object PatternMatching {

  //Q0
  case class Player(name: String, jerseyNumber: Int) {
    // TODO Shouldn't need to override toString for any of this question
    override def toString: String = name + " " + jerseyNumber
  }


  def matchAPlayer(player: Player): String = {
    // TODO This match doesn't really match the Player. Try using Player(name, number) in place of the _
    player match {
      case _ => player.toString
    }
  }

  def matchAPlayerWithJerseyNumberGreaterThan3(player: Player): String = {
    // TODO Likewise here
    player match {
      case _ if player.jerseyNumber > 3 => player.toString
      case _ => ""
    }
  }

  def matchAPlayerWithJerseyNumberEquals3(player: Player): Int = {
    player match {
      case Player(_, 3) => player.jerseyNumber
      case _ => -1
    }
  }

  /**
    * Each of the following would have a definition of Leaf/Node classes.
    * Hint: You can nest classes in Scala and it's easy to give each question a separate class.
    */
  //Q5


  def leafSum(list: List[Any]): Int = {
    list match {
      case Nil => 0
      case head :: tail =>
        val sumOfHead: Int = head match {
          case x: List[Any] => leafSum(x)
          case x: Int => x
          case _ => 0
        }
        sumOfHead + leafSum(tail)
    }
  }

  //Q6

  //TODO can you make BinaryTree with its leaf and node separate from the tree with multiple children, to avoid incomplete match warnings
  sealed abstract class BinaryTree

  case class Leaf(value: Int) extends BinaryTree

  case class Node(left: BinaryTree, right: BinaryTree) extends BinaryTree

  def leafSum(binaryTree: BinaryTree): Int = {
    binaryTree match {
      case x: Leaf => x.value
      case x: Node => leafSum(x.left) + leafSum(x.right)
    }
  }

  //Q7

  case class NodeWithMultipleChildren(children: BinaryTree*) extends BinaryTree


  def leafSumForNodeWithMultipleChildren(binaryTree: BinaryTree): Int = {
    binaryTree match {
      case x: Leaf => x.value
      case x: NodeWithMultipleChildren =>
        //TODO you can use .map().sum to simplify this
        x.children.foldLeft(0)((acc, restOfChildren) => acc + leafSumForNodeWithMultipleChildren(restOfChildren))
    }
  }


  //Q8

  //TODO The question requests that we not use an operator trait with hardcoded operations. Have you looked at passing and storing functions?
  trait MathOperator {
    def apply(x: Int, y: Int): Int
  }

  object Plus extends MathOperator {
    override def apply(x: Int, y: Int): Int = x + y
  }

  object Minus extends MathOperator {
    override def apply(x: Int, y: Int): Int = x - y
  }

  object Divide extends MathOperator {
    override def apply(x: Int, y: Int): Int = x / y
  }

  object Multiply extends MathOperator {
    override def apply(x: Int, y: Int): Int = x * y
  }

  sealed abstract class MathOperationTree

  case class ValueLeaf(value: Int) extends MathOperationTree

  case class OperatorNode(operator: MathOperator, children: MathOperationTree*) extends MathOperationTree

  def eval(mathOperationTree: MathOperationTree): Int = {
    mathOperationTree match {
      //TODO this case is unneeded if you match the node's children into a list
      case OperatorNode(_: MathOperator) => 0 //exception where there is no children underneath an operator node
      //TODO you can match the node's children into a list with the pattern children @ _* instead of having a special case for one item and another for multiple children
      case OperatorNode(Minus, child: MathOperationTree) => -eval(child) //negate case
      case x: OperatorNode => x.children.map(child => eval(child)).reduceLeft((a, b) => x.operator(a, b))
      case x: ValueLeaf => x.value
    }
  }
}
